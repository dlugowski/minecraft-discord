#!/bin/bash
username=$1 ;# username of player that logged in or out
status=$2 ;# 'in' or 'out'
webhook=`cat webhook.txt`
curl -X POST -H "Content-Type: application/json" -d '{"username":"minecraft_logins","content":"'${username}' just logged '${status}' the server"}' ${webhook}
